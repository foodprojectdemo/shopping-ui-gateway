package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Cart;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Customer;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Order;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.RestaurantService;
import com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order.OrderGateway.CheckoutParams;
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.ClientErrorResponseException;
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.ForbiddenException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
class OrderServiceAdapter implements OrderService {
    private final OrderGateway orderGateway;
    private final RestaurantService restaurantService;

    @Override
    public Mono<String> createCart() {
        return orderGateway.createCart()
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Mono<Cart> getCart(String cartId) {
        return orderGateway.getCart(cartId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty())
                .flatMap(this::toCart);
    }

    @Override
    public Mono<Void> addDishToCart(String cartId, Long dishId, int quantity) {
        return orderGateway.addDishToCart(cartId, dishId, quantity)
                .onErrorMap(this::isAnotherRestaurantDishException, throwable -> new AnotherRestaurantDishException())
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<Void> changeCartItemQuantity(String cartId, int index, int quantity) {
        return orderGateway.changeCartItemQuantity(cartId, index, quantity)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<Void> removeCartItem(String cartId, int index) {
        return orderGateway.removeCartItem(cartId, index);
    }

    @Override
    public Mono<Void> applyPromoCode(String cartId, String promoCodeId) {
        return orderGateway.applyPromoCode(cartId, promoCodeId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<Void> cancelPromoCode(String cartId) {
        return orderGateway.cancelPromoCode(cartId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<Void> placePreOrder(String cartId) {
        return orderGateway.placePreOrder(cartId);
    }

    @Override
    public Mono<Cart> getPreOrder(String cartId) {
        return orderGateway.getPreOrder(cartId)
                .flatMap(this::toCart)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<String> signUpCustomer() {
        return orderGateway.signUpCustomer()
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }

    @Override
    public Mono<Customer> getCustomer(String customerId) {
        return orderGateway.getCustomer(customerId);
    }

    @Override
    public Mono<String> placeOrder(Mono<String> cartId, String customerId, CheckoutRequest request) {
        return cartId
                .flatMap(_cartId -> orderGateway.placeOrder(CheckoutParams
                        .builder()
                        .customerId(customerId)
                        .cartId(_cartId)
                        .paymentMethodId(UUID.randomUUID().toString())
                        .shippingMethodId(UUID.randomUUID().toString())
                        .shippingAddressStreet(request.getStreet())
                        .shippingAddressHouse(request.getHouse())
                        .shippingAddressFlat(request.getFlat())
                        .contactInformationName(request.getName())
                        .contactInformationEmail(request.getEmail())
                        .contactInformationPhone(request.getPhone())
                        .build()
                ));
    }

    @Override
    public Flux<Order> getOrders(String customerId) {
        return orderGateway.getOrders(customerId)
                .flatMap(orderDto -> restaurantService.restaurant(orderDto.getRestaurantId())
                        .map(orderDto::toOrder))
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable));
    }


    private Mono<Cart> toCart(CartDto cartDto) {
        return Flux.fromStream(
                Stream.concat(
                        cartDto.getItems().stream(),
                        cartDto.getGifts().stream()
                )
                        .map(CartItemDto::getDishId)
                        .distinct()
        ).parallel()
                .runOn(Schedulers.boundedElastic())
                .flatMap(restaurantService::dish)
                .sequential()
                .collectMap(Dish::getId, dish -> dish)
                .map(dishMap ->
                        Cart.builder()
                                .items(cartItems(cartDto.getItems(), dishMap))
                                .gifts(cartItems(cartDto.getGifts(), dishMap))
                                .total(cartDto.getTotal())
                                .subTotal(cartDto.getSubTotal())
                                .discount(cartDto.getDiscount())
                                .promoCodeId(cartDto.getPromoCodeId())
                                .build()
                );
    }

    private Mono<Cart> toCart(PreOrderDto preOrderDto) {
        return Flux.fromStream(
                preOrderDto.getItems().stream()
                        .map(CartItemDto::getDishId)
        ).parallel()
                .runOn(Schedulers.boundedElastic())
                .flatMap(restaurantService::dish)
                .sequential()
                .collectMap(Dish::getId, dish -> dish)
                .map(dishMap ->
                        Cart.builder()
                                .items(cartItems(preOrderDto.getItems(), dishMap))
                                .gifts(Collections.emptyList())
                                .total(preOrderDto.getTotal())
                                .subTotal(preOrderDto.getSubTotal())
                                .discount(preOrderDto.getDiscount())
                                .build()
                );
    }

    private List<Cart.Item> cartItems(Collection<CartItemDto> items, Map<Long, Dish> dishMap) {
        return items.stream().map(item ->
                Cart.Item.builder()
                        .dishId(item.getDishId())
                        .quantity(item.getQuantity())
                        .price(item.getPrice())
                        .totalAmount(item.getTotalAmount())
                        .dish(dishMap.get(item.getDishId()))
                        .build()
        ).collect(Collectors.toList());
    }

    private boolean isAnotherRestaurantDishException(Throwable throwable) {
        return throwable instanceof ForbiddenException &&
                ((ForbiddenException) throwable).getErrorCode().equals("ANOTHER_RESTAURANT_DISH");
    }

    private boolean isNotClientError(Throwable throwable) {
        return throwable instanceof ClientErrorResponseException;
    }
}
