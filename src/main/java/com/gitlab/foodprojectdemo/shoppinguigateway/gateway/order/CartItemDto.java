package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.NonNull;
import lombok.Value;

@Value
class CartItemDto {
    @NonNull Long dishId;
    @NonNull String price;
    @NonNull String totalAmount;
    @NonNull Integer quantity;
}
