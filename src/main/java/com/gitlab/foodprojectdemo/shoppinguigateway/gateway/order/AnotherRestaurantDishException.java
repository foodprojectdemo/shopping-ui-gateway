package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

public class AnotherRestaurantDishException extends RuntimeException {
    public AnotherRestaurantDishException() {
        super("You cannot add dishes from different restaurants into the cart. You can checkout order or clear your cart.");
    }
}
