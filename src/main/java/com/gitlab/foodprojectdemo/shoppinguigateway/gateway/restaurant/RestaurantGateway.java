package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Category;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoCode;
import com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant.RestaurantGateway.Properties;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@EnableConfigurationProperties(Properties.class)
class RestaurantGateway {

    @Value
    @ConstructorBinding
    @ConfigurationProperties("restaurant-service")
    static class Properties {
        @NonNull String url;
        @NonNull String apiBaseUri;
        @NonNull String restaurantsUri;
        @NonNull String restaurantUri;
        @NonNull String restaurantDishesUri;
        @NonNull String restaurantCategoriesUri;
        @NonNull String dishUri;
        @NonNull String promoActionsUri;
        @NonNull String promoCodesUri;

        public String getBaseUri() {
            return url + apiBaseUri;
        }
    }

    private final WebClient webClient;
    private final Properties properties;

    RestaurantGateway(Builder builder, Properties properties) {
        this.webClient = builder.baseUrl(properties.getBaseUri()).build();
        this.properties = properties;
    }

    Flux<Restaurant> restaurants() {
        return webClient.get()
                .uri(properties.getRestaurantsUri())
                .retrieve()
                .bodyToFlux(Restaurant.class);
    }

    Mono<Restaurant> restaurant(Long restaurantId) {
        return webClient.get()
                .uri(properties.getRestaurantUri(), restaurantId)
                .retrieve()
                .bodyToMono(Restaurant.class);
    }

    Flux<Category> categories(Long restaurantId) {
        return webClient.get()
                .uri(properties.getRestaurantCategoriesUri(), restaurantId)
                .retrieve()
                .bodyToFlux(Category.class);
    }

    Flux<Dish> dishes(Long restaurantId) {
        return webClient.get()
                .uri(properties.getRestaurantDishesUri(), restaurantId)
                .retrieve()
                .bodyToFlux(Dish.class);
    }

    Mono<Dish> dish(Long dishId) {
        return webClient.get()
                .uri(properties.getDishUri(), dishId)
                .retrieve()
                .bodyToMono(Dish.class);
    }

    Flux<PromoActionDto> promoActions(Long restaurantId) {
        return webClient.get()
                .uri(properties.getPromoActionsUri(), restaurantId)
                .retrieve()
                .bodyToFlux(PromoActionDto.class);
    }

    Flux<PromoCode> promoCodes(Long restaurantId) {
        return webClient.get()
                .uri(properties.getPromoCodesUri(), restaurantId)
                .retrieve()
                .bodyToFlux(PromoCode.class);
    }
}
