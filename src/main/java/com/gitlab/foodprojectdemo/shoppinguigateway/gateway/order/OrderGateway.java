package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@Slf4j
@Component
@EnableConfigurationProperties(OrderGatewayProperties.class)
class OrderGateway {
    private final WebClient webClient;
    private final OrderGatewayProperties properties;

    OrderGateway(WebClient.Builder builder, OrderGatewayProperties properties) {
        this.webClient = builder.baseUrl(properties.getUrl() + properties.getApiBaseUri()).build();
        this.properties = properties;
    }

    @Value
    static class IdDto {
        String id;
    }

    Mono<String> createCart() {
        return webClient.post().uri(properties.getCartsUri())
                .retrieve()
                .bodyToMono(IdDto.class)
                .map(IdDto::getId);
    }

    Mono<CartDto> getCart(String cartId) {
        return webClient.get().uri(properties.getCartUri(), cartId)
                .retrieve()
                .bodyToMono(CartDto.class);

    }

    Mono<Void> addDishToCart(String cartId, Long dishId, int quantity) {
        return webClient.post()
                .uri(properties.getCartItemsUri(), cartId)
                .bodyValue(Map.of("dishId", dishId, "quantity", quantity))
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<Void> changeCartItemQuantity(String cartId, int index, int quantity) {
        return webClient.patch().uri(properties.getCartItemUri(), cartId, index)
                .bodyValue(Map.of("quantity", quantity))
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<Void> removeCartItem(String cartId, int index) {
        return webClient.delete().uri(properties.getCartItemUri(), cartId, index)
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<Void> applyPromoCode(String cartId, String promoCodeId) {
        return webClient.put()
                .uri(properties.getCartPromoCode(), cartId)
                .bodyValue(Map.of("promoCodeId", promoCodeId))
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<Void> cancelPromoCode(String cartId) {
        return webClient.delete()
                .uri(properties.getCartPromoCode(), cartId)
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<Void> placePreOrder(String cartId) {
        return webClient.post()
                .uri(properties.getCartPreOrder(), cartId)
                .retrieve()
                .toBodilessEntity()
                .then();
    }

    Mono<PreOrderDto> getPreOrder(String cartId) {
        return
                webClient.get()
                        .uri(properties.getCartPreOrder(), cartId)
                        .retrieve()
                        .bodyToMono(PreOrderDto.class);
    }

    Mono<String> signUpCustomer() {
        return webClient.post().uri(properties.getCustomersUri())
                .retrieve()
                .bodyToMono(IdDto.class)
                .map(IdDto::getId);
    }

    Mono<Customer> getCustomer(String customerId) {
        return webClient.get().uri(properties.getCustomerUri(), customerId)
                .retrieve()
                .bodyToMono(Customer.class);
    }

    @Value
    @Builder
    static class CheckoutParams {
        @NonNull String cartId;
        @NonNull String customerId;
        @NonNull String paymentMethodId;
        @NonNull String shippingMethodId;
        @NonNull String shippingAddressStreet;
        @NonNull String shippingAddressHouse;
        int shippingAddressFlat;
        @NonNull String contactInformationName;
        @NonNull String contactInformationPhone;
        @NonNull String contactInformationEmail;
    }

    Mono<String> placeOrder(CheckoutParams params) {
        return webClient.post().uri(properties.getOrdersUri()).bodyValue(params)
                .retrieve()
                .bodyToMono(IdDto.class)
                .map(IdDto::getId);
    }

    Flux<OrderDto> getOrders(String customerId) {
        return webClient.get().uri(uriBuilder ->
                uriBuilder.path(properties.getOrdersUri()).queryParam("customerId", customerId).build()
        )
                .retrieve()
                .bodyToFlux(OrderDto.class);
    }

}
