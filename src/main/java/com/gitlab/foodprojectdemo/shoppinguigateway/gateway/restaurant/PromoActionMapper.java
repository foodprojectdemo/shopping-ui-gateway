package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.GiftPromoAction.Item;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
class PromoActionMapper {

    Mono<PromoAction> DtoToPromoAction(PromoActionDto dto, Mono<Map<Long, Dish>> dishes) {
        if (dto instanceof WithMapper) {
            return Mono.just(((WithMapper<?>) dto).toPromoAction());
        } else if (dto instanceof GiftPromoActionDto) {
            return dishes.flatMap(dishMap -> Mono.justOrEmpty(dtoToGiftPromoAction((GiftPromoActionDto) dto, dishMap)));
        }
        throw new IllegalStateException("It`s not available");
    }

    private GiftPromoAction dtoToGiftPromoAction(GiftPromoActionDto dto, Map<Long, Dish> dishMap) {
        var gifts = dto.getGifts().stream()
                .map(item -> {
                    if (dishMap.containsKey(item.getDishId())) {
                        return new Item(item.getAmount(), dishMap.get(item.getDishId()));
                    } else {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (gifts.isEmpty()) {
            return null;
        } else {
            return new GiftPromoAction(
                    dto.getId(),
                    dto.getRestaurantId(),
                    gifts
            );
        }
    }
}
