package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties("order-service")
class OrderGatewayProperties {
    String url;
    String apiBaseUri;
    String cartsUri;
    String cartUri;
    String cartItemsUri;
    String cartItemUri;
    String cartPromoCode;
    String cartPreOrder;
    String ordersUri;
    String customersUri;
    String customerUri;
}
