package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
class CartDto {
    @NonNull String id;
    @NonNull List<CartItemDto> items;
    @NonNull List<CartItemDto> gifts;
    @NonNull String total;
    @NonNull String subTotal;
    @NonNull String discount;
    String promoCodeId;
}
