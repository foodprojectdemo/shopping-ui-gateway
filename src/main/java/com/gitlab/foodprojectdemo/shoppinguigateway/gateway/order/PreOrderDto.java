package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
class PreOrderDto {
    @NonNull Long restaurantId;
    @NonNull List<CartItemDto> items;
    @NonNull String total;
    @NonNull String subTotal;
    @NonNull String discount;
}
