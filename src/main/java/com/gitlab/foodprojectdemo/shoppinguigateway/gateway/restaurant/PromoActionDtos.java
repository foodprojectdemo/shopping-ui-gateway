package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Value;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;

import java.util.Collection;
import java.util.stream.Collectors;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbsoluteDiscountPromoActionDto.class, name = "absolute_promo_action"),
        @JsonSubTypes.Type(value = PercentDiscountPromoActionDto.class, name = "percent_promo_action"),
        @JsonSubTypes.Type(value = GiftPromoActionDto.class, name = "gift_promo_action"),
})
interface PromoActionDto {
}

interface WithMapper<T extends PromoAction> extends PromoActionDto {
    T toPromoAction();
}


@Value
class AbsoluteDiscountPromoActionDto implements WithMapper<AbsoluteDiscountPromoAction> {

    @Value
    static class Item {
        Integer amount;
        Integer discount;
    }

    Long id;
    Long restaurantId;
    Collection<Item> discounts;

    @Override
    public AbsoluteDiscountPromoAction toPromoAction() {
        return new AbsoluteDiscountPromoAction(
                id,
                restaurantId,
                discounts.stream()
                        .map(item -> new AbsoluteDiscountPromoAction.Item(item.amount, item.discount))
                        .collect(Collectors.toList())
        );
    }
}

@Value
class PercentDiscountPromoActionDto implements WithMapper<PercentDiscountPromoAction> {
    @Value
    static class Item {
        Integer amount;
        Integer percent;
    }

    Long id;
    Long restaurantId;
    Collection<Item> discounts;

    @Override
    public PercentDiscountPromoAction toPromoAction() {
        return new PercentDiscountPromoAction(
                id,
                restaurantId,
                discounts.stream()
                        .map(item -> new PercentDiscountPromoAction.Item(item.amount, item.percent))
                        .collect(Collectors.toList())
        );
    }
}

@Value
class GiftPromoActionDto implements PromoActionDto {
    @Value
    static class Item {
        Integer amount;
        Long dishId;
    }

    Long id;
    Long restaurantId;
    Collection<Item> gifts;
}