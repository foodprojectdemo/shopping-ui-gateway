package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Category;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.RestaurantService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoCode;
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.ClientErrorResponseException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@AllArgsConstructor
class RestaurantServiceAdapter implements RestaurantService {
    private final RestaurantGateway restaurantGateway;
    private final PromoActionMapper mapper;

    @Override
    public Mono<Page<Restaurant>> restaurants(Pageable pageable) {
        var restaurants = restaurantGateway.restaurants().cache();
        var restaurantsOnPage = restaurants
                .skip(pageable.getOffset())
                .take(pageable.getPageSize())
                .collectList();
        var totalCount = restaurants.count();

        return restaurantsOnPage.zipWith(totalCount,
                (pageItems, total) -> (Page<Restaurant>) new PageImpl<>(pageItems, pageable, total)
        )
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Mono<Restaurant> restaurant(Long id) {
        return restaurantGateway.restaurant(id)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Flux<Category> categories(Long restaurantId) {
        return restaurantGateway.categories(restaurantId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Flux<Dish> dishes(Long restaurantId) {
        return restaurantGateway.dishes(restaurantId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Mono<Dish> dish(Long dishId) {
        return restaurantGateway.dish(dishId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Flux<PromoAction> promoActions(Long restaurantId) {
        var DTOs = restaurantGateway.promoActions(restaurantId);
        var dishes = dishes(restaurantId).collectMap(Dish::getId).cache();

        return DTOs
                .flatMap(dto -> mapper.DtoToPromoAction(dto, dishes))
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    @Override
    public Flux<PromoCode> promoCodes(Long restaurantId) {
        return restaurantGateway.promoCodes(restaurantId)
                .doOnError(this::isNotClientError, throwable -> log.error(throwable.getMessage(), throwable))
                .onErrorResume(throwable -> Mono.empty());
    }

    private boolean isNotClientError(Throwable throwable) {
        return throwable instanceof ClientErrorResponseException;
    }
}
