package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import lombok.NonNull;
import lombok.Value;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Order;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;

import java.util.List;

@Value
class OrderDto {

    @NonNull String id;
    @NonNull String customerId;
    @NonNull Long restaurantId;
    @NonNull List<Order.Item> items;
    @NonNull String total;
    @NonNull String subTotal;
    @NonNull String discount;
    @NonNull String status;
    @NonNull Order.Address address;
    @NonNull Order.ContactInformation contactInformation;
//    @NonNull ZonedDateTime createdAt;

    public Order toOrder(Restaurant restaurant) {
        return new Order(id,
                customerId,
                restaurantId,
                restaurant,
                items,
                total,
                subTotal,
                discount,
                status,
                address,
                contactInformation
//                createdAt
        );
    }
}
