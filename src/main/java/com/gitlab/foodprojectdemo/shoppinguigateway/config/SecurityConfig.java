package com.gitlab.foodprojectdemo.shoppinguigateway.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository;
import com.gitlab.foodprojectdemo.shoppinguigateway.ui.SignUpCustomerService;
import reactor.core.publisher.Mono;

@Slf4j
@AllArgsConstructor
@EnableWebFluxSecurity
class SecurityConfig {

    private final SignUpCustomerService signUpCustomerService;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    MapReactiveUserDetailsService userDetailsService() {
        UserDetails user = User
                .withUsername("user")
                .password(passwordEncoder().encode("password"))
                .roles("USER")
                .build();
        return new MapReactiveUserDetailsService(user);
    }

    @Bean
    WebSessionServerSecurityContextRepository webSessionServerSecurityContextRepository() {
        return new WebSessionServerSecurityContextRepository();
    }

    @Bean
    SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http, ServerAuthenticationSuccessHandler successHandler) {
        return http.authorizeExchange()
                .pathMatchers("/my/**").authenticated()
                .anyExchange().permitAll()
                .and()
                .oauth2Login()
                .authenticationSuccessHandler(successHandler)
                .and()
                .formLogin()
                .and()
                .build();
    }


    @Bean
    ServerAuthenticationSuccessHandler successHandler() {
        return new RedirectServerAuthenticationSuccessHandler() {
            @Override
            public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
                return signUpCustomerService.signUp(authentication)
                        .then(super.onAuthenticationSuccess(webFilterExchange, authentication));
            }
        };
    }

}
