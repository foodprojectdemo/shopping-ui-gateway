package com.gitlab.foodprojectdemo.shoppinguigateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.WebClientErrorHandlerAutoConfiguration;

@Slf4j
@Configuration
@ImportAutoConfiguration({WebClientAutoConfiguration.class, WebClientErrorHandlerAutoConfiguration.class})
public class WebClientConfig {

}
