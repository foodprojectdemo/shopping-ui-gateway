package com.gitlab.foodprojectdemo.shoppinguigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("com.gitlab.foodprojectdemo.shoppinguigateway")
public class ShoppingUiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingUiGatewayApplication.class, args);
    }

}
