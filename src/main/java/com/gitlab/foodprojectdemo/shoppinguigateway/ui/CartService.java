package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.server.WebSession;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Cart;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
class CartService {
    public static final String CART_ID_KEY = "cartId";
    private final OrderService orderService;

    public Mono<Cart> getCart(WebSession session) {
        return getCartId(session).flatMap(orderService::getCart)
                // if the cart was not found then try to create a new cart
                .switchIfEmpty(newCartId(session).flatMap(orderService::getCart));
    }

    private Mono<String> getCartId(WebSession session) {
        return Mono.justOrEmpty((String) session.getAttribute(CART_ID_KEY))
                .switchIfEmpty(newCartId(session));
    }

    private Mono<String> newCartId(WebSession session) {
        return orderService.createCart().map(cartId -> saveCartId(cartId, session));
    }

    private String saveCartId(String cartId, WebSession session) {
        session.getAttributes().put(CART_ID_KEY, cartId);
        return cartId;
    }

}
