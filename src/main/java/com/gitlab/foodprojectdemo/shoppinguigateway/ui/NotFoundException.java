package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Page not found");
    }
}
