package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.User;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.UserRepository;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@AllArgsConstructor
class CustomerResolver {
    private final UserRepository userRepository;

    Mono<String> getCustomerIdByAuthentication(Authentication authentication) {
        if (authentication instanceof OAuth2AuthenticationToken) {
            var id = getUserId((OAuth2AuthenticationToken) authentication);
            return userRepository.findById(id).map(User::getCustomerId);
        } else if (authentication instanceof PreAuthenticatedAuthenticationToken) {
            return Mono.just((String) authentication.getDetails());
        }

        return Mono.empty();
    }

    User.CompositeUserId getUserId(OAuth2AuthenticationToken token) {
        var providerId = token.getAuthorizedClientRegistrationId();
        var userIdAttr = getUserIdAttributeByProvider(providerId);
        var userId = token.getPrincipal().getAttribute(userIdAttr);
        return new User.CompositeUserId(providerId, userId);
    }

    private String getUserIdAttributeByProvider(String providerId) {
        switch (providerId) {
            case "google":
                return "sub";
            case "github":
                return "id";
            default:
                throw new RuntimeException("Unknown provider: " + providerId);
        }
    }
}
