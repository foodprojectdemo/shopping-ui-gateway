package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.User;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.UserRepository;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@AllArgsConstructor
public class SignUpCustomerService {
    private final OrderService orderService;
    private final UserRepository userRepository;
    private final CustomerResolver customerResolver;

    public Mono<User> signUp(Authentication authentication) {
        if (authentication instanceof OAuth2AuthenticationToken) {
            var id = customerResolver.getUserId((OAuth2AuthenticationToken) authentication);

            return userRepository.findById(id)
                    .switchIfEmpty(createCustomer(id));
        }
        return Mono.empty();
    }

    private Mono<User> createCustomer(User.CompositeUserId id) {
        return orderService.signUpCustomer()
                .flatMap(customerId -> userRepository.save(new User(id, customerId)));
    }
}
