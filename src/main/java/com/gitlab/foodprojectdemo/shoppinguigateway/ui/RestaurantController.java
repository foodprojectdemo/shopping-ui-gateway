package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.RestaurantService;
import reactor.core.publisher.Mono;

@Slf4j
@Controller
class RestaurantController {
    private final RestaurantService restaurantService;
    private final int restaurantsPerPage;

    RestaurantController(RestaurantService restaurantService, @Value("${app.restaurants-pre-page:16}") int restaurantsPerPage) {
        this.restaurantService = restaurantService;
        this.restaurantsPerPage = restaurantsPerPage;
    }

    @GetMapping("/")
    String restaurants(@RequestParam(required = false, defaultValue = "0") int page, Model model) {
        var restaurants = restaurantService.restaurants(PageRequest.of(page, restaurantsPerPage));
        model.addAttribute("page", restaurants);

        return "restaurants";
    }

    @GetMapping("/restaurant/{id}")
    String restaurant(@PathVariable Long id, final Model model) {
        var restaurant = restaurantService.restaurant(id).switchIfEmpty(Mono.error(new NotFoundException()));
        var categories = restaurantService.categories(id);
        var dishes = restaurantService.dishes(id);
        var promoActions = restaurantService.promoActions(id);
        var promoCodes = restaurantService.promoCodes(id);

        model.addAttribute("restaurant", restaurant);
        model.addAttribute("categories", categories);
        model.addAttribute("dishes", dishes);
        model.addAttribute("promoActions", promoActions);
        model.addAttribute("promoCodes", promoCodes);

        return "restaurant";
    }

}
