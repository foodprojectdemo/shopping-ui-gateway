package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
class AutoLoginService {
    private final ServerSecurityContextRepository securityContextRepository;

    Mono<Void> autoLogin(String customerId, String name, ServerWebExchange serverWebExchange) {
        //TODO: create custom token
        var token = new PreAuthenticatedAuthenticationToken(
                name,
                "",
                AuthorityUtils.createAuthorityList("ROLE_USER")
        );
        token.setDetails(customerId);

        return securityContextRepository.save(serverWebExchange, new SecurityContextImpl(token));
    }
}
