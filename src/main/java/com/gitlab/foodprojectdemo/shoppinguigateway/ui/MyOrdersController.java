package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;

@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/my/orders")
class MyOrdersController {
    private final OrderService orderService;
    private final CustomerResolver customerResolver;

    @GetMapping
    String orders(Model model, Authentication authentication) {
        var orders = customerResolver.getCustomerIdByAuthentication(authentication)
                .flatMapMany(orderService::getOrders);

        model.addAttribute("orders", orders);

        return "my";
    }

}
