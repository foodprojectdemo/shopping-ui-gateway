package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;
import reactor.core.publisher.Mono;

@Value
class AddDishToCartRequest {
    Long dishId;
}

@Value
class ChangeCartItemQuantityRequest {
    int index;
    int quantity;
}

@Value
class RemoveCartItemRequest {
    int index;
}

@Value
class ApplyPromoCodeToCartRequest {
    String promoCodeId;
}

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/cart")
@SessionAttributes("cartId")
class AjaxCartController {
    private final OrderService orderService;

    @ModelAttribute("cartId")
    private Mono<String> cartId() {
        return orderService.createCart().cache()
                .switchIfEmpty(Mono.error(new RuntimeException("The service is not available. Please try again later.")));
    }


    @PostMapping("add-to-cart")
    Mono<AjaxAnswer> addToCart(@ModelAttribute("cartId") Mono<String> cartId, AddDishToCartRequest request) {
        return cartId.flatMap(id -> orderService.addDishToCart(id, request.getDishId(), 1))
                .doOnError(throwable -> log.info("{}", throwable))
                .transform(this::toAjaxAnswer);
    }

    @PostMapping("change")
    Mono<AjaxAnswer> change(@ModelAttribute("cartId") Mono<String> cartId, ChangeCartItemQuantityRequest request) {
        return cartId.flatMap(id -> orderService.changeCartItemQuantity(id, request.getIndex(), request.getQuantity()))
                .transform(this::toAjaxAnswer);
    }

    @PostMapping("remove")
    Mono<AjaxAnswer> remove(@ModelAttribute("cartId") Mono<String> cartId, RemoveCartItemRequest request) {
        return cartId.flatMap(id -> orderService.removeCartItem(id, request.getIndex()))
                .transform(this::toAjaxAnswer);
    }

    @PostMapping("applypromocode")
    Mono<AjaxAnswer> applyPromoCode(@ModelAttribute("cartId") Mono<String> cartId, ApplyPromoCodeToCartRequest request) {
        return cartId.flatMap(id -> orderService.applyPromoCode(id, request.getPromoCodeId()))
                .transform(this::toAjaxAnswer);
    }

    @PostMapping("cancelpromocode")
    Mono<AjaxAnswer> cancelPromoCode(@ModelAttribute("cartId") Mono<String> cartId) {
        return cartId.flatMap(orderService::cancelPromoCode)
                .transform(this::toAjaxAnswer);
    }

    @PostMapping("makeReservation")
    Mono<AjaxAnswer> makeReservation(@ModelAttribute("cartId") Mono<String> cartId) {
        return cartId.flatMap(orderService::placePreOrder)
                .transform(this::toAjaxAnswer);
    }


    private Mono<AjaxAnswer> toAjaxAnswer(Mono<Void> result) {
        return result.thenReturn(AjaxAnswer.success())
                .onErrorResume(throwable -> Mono.just(AjaxAnswer.failure(throwable)));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private Mono<AjaxAnswer> exception(Exception e) {
        return Mono.just(AjaxAnswer.failure(e));
    }
}

