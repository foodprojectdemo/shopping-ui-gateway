package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.WebSession;


@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/cart")
class CartController {
    private final CartService cartService;

    @GetMapping
    String get(WebSession session, Model model) {
        model.addAttribute("cart", cartService.getCart(session));
        return "cart";
    }

}
