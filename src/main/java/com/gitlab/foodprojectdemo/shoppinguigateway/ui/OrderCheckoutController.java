package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.server.ServerWebExchange;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService.CheckoutRequest;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/checkout")
@SessionAttributes("cartId")
@AllArgsConstructor
class OrderCheckoutController {
    private final OrderService orderService;
    private final CustomerResolver customerResolver;
    private final AutoLoginService autoLoginService;

    @GetMapping
    Mono<String> checkout(@ModelAttribute("cartId") Mono<String> cartId, Model model) {
        var preOrder = cartId.flatMap(orderService::getPreOrder);
        model.addAttribute("preOrder", preOrder);

        return preOrder.map(value -> "checkout")
                .switchIfEmpty(Mono.just("redirect:/cart"));
    }

    @PostMapping
    Mono<String> placeOrder(@ModelAttribute("cartId") Mono<String> cartId,
                            CheckoutRequest request,
                            ServerWebExchange serverWebExchange,
                            Authentication authentication) {
        var orderId = getCustomerIdByAuthenticationOrSignUpCustomer(authentication)
                .flatMap(customerId -> {
                            var _orderId = orderService.placeOrder(cartId, customerId, request);

                            return autoLoginService.autoLogin(customerId, request.getName(), serverWebExchange).then(_orderId);
                        }
                );

        return orderId.map(_orderId -> "redirect:/checkout/success/" + _orderId);
    }

    private Mono<String> getCustomerIdByAuthenticationOrSignUpCustomer(Authentication authentication) {
        if (authentication != null) {
            return customerResolver.getCustomerIdByAuthentication(authentication);
        } else {
            return orderService.signUpCustomer();
        }
    }


    @GetMapping("success/{id}")
    String success(@PathVariable("id") String orderId, Model model) {
        model.addAttribute("orderId", orderId);
        return "success";
    }

}
