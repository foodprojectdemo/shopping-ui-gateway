package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.Value;

@Value
class AjaxAnswer {
    private static final String SUCCESS = "SUCCESS";
    private static final String FAILURE = "FAILURE";
    String status;
    Object data;

    @Value
    static class Error {
        String message;
    }

    private AjaxAnswer(String status, Object data) {
        this.status = status;
        this.data = data;
    }

    public static AjaxAnswer success() {
        return new AjaxAnswer(SUCCESS, null);
    }

    public static AjaxAnswer failure(Throwable throwable) {
        return new AjaxAnswer(FAILURE, new Error(throwable.getMessage()));
    }
}
