package com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo;

import lombok.NonNull;
import lombok.Value;

@Value
public class AbsoluteDiscountPromoCode implements PromoCode {
    @NonNull Long restaurantId;
    @NonNull String code;
    @NonNull Integer discount;
}
