package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@Builder
public class Restaurant {

    public enum WeekDay implements Comparable<WeekDay> {
        Mon, Tue, Wen, Thu, Fri, Sat, Sun
    }

    @Value
    @Builder
    public static class LegalData {
        @NonNull String address;
        @NonNull String inn;
        @NonNull String name;
        @NonNull String ogrn;
    }

    @NonNull Long id;
    @NonNull String name;
    @NonNull String information;
    @NonNull String logo;
    @NonNull String openDays;
    @NonNull String openFrom;
    @NonNull String openTo;
    @NonNull LegalData legalData;
    @NonNull Set<String> cuisines;

    public Set<WeekDay> getOpenDays() {
        return Arrays.stream(openDays.split(","))
                .map(day -> {
                            try {
                                return WeekDay.values()[Integer.parseInt(day.trim()) - 1];
                            } catch (RuntimeException e) {
                                return null;
                            }
                        }
                )
                .filter(Objects::nonNull)
                .sorted()
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
