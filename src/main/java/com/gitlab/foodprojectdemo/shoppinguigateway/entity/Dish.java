package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.NonNull;
import lombok.Value;

@Value
public class Dish {
    @NonNull Long id;
    @NonNull Long restaurantId;
    @NonNull Long categoryId;
    @NonNull String name;
    @NonNull String description;
    @NonNull Integer amount;
    @NonNull String imageUrl;
}
