package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Cart {
    @Value
    @Builder
    public static class Item {
        @NonNull Long dishId;
        @NonNull Integer quantity;
        @NonNull String price;
        @NonNull String totalAmount;
        @NonNull Dish dish;
    }

    @NonNull List<Item> items;
    @NonNull List<Item> gifts;
    @NonNull String total;
    @NonNull String subTotal;
    @NonNull String discount;

    String promoCodeId;

    public boolean isEmpty() {
        return items.isEmpty();
    }
}
