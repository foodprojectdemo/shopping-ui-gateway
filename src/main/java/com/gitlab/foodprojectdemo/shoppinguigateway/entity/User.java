package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Document("user")
@Getter
public class User {
    @Value
    public static class CompositeUserId implements Serializable {
        private static final long serialVersionUID = 4158117447941901559L;
        @NonNull String providerId;
        @NonNull Object userId;
    }

    @Id
    private CompositeUserId id;
    private String customerId;
}
