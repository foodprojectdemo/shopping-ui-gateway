package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import reactor.core.publisher.Mono;

public interface UserRepository {
    Mono<User> save(User user);

    Mono<User> findById(User.CompositeUserId id);
}
