package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoCode;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RestaurantService {
    Mono<Page<Restaurant>> restaurants(Pageable pageable);

    Mono<Restaurant> restaurant(Long id);

    Flux<Category> categories(Long restaurantId);

    Flux<Dish> dishes(Long restaurantId);

    Mono<Dish> dish(Long dishId);

    Flux<PromoAction> promoActions(Long restaurantId);

    Flux<PromoCode> promoCodes(Long restaurantId);
}
