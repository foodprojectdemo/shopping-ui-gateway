package com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo;

import lombok.NonNull;
import lombok.Value;

import java.util.Collection;

@Value
public class PercentDiscountPromoAction implements PromoAction {
    @Value
    public static class Item {
        @NonNull Integer amount;
        @NonNull Integer percent;
    }

    @NonNull Long id;
    @NonNull Long restaurantId;
    @NonNull Collection<Item> discounts;
}
