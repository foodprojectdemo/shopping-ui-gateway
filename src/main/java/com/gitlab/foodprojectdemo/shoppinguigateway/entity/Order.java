package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.NonNull;
import lombok.Value;

import java.util.Collection;

@Value
public class Order {
    @Value
    public static class Item {
        @NonNull String name;
        @NonNull Integer quantity;
        @NonNull String price;
        @NonNull String totalAmount;
    }

    @Value
    public static class Address {
        @NonNull String street;
        @NonNull String house;
        @NonNull Integer flat;
    }

    @Value
    public static class ContactInformation {
        @NonNull String name;
        @NonNull String phone;
        @NonNull String email;
    }

    @NonNull String id;
    @NonNull String customerId;
    @NonNull Long restaurantId;
    @NonNull Restaurant restaurant;
    @NonNull Collection<Item> items;
    @NonNull String total;
    @NonNull String subTotal;
    @NonNull String discount;
    @NonNull String status;
    @NonNull Address address;
    @NonNull ContactInformation contactInformation;
//    ZonedDateTime createdAt;
}
