package com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo;

import lombok.NonNull;
import lombok.Value;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;

import java.util.Collection;

@Value
public class GiftPromoAction implements PromoAction {
    @Value
    public static class Item {
        @NonNull Integer amount;
        @NonNull Dish dish;
    }

    @NonNull Long id;
    @NonNull Long restaurantId;
    @NonNull Collection<Item> gifts;
}
