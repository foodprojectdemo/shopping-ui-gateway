package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.NonNull;
import lombok.Value;

@Value
public class Customer {
    @NonNull String id;
}
