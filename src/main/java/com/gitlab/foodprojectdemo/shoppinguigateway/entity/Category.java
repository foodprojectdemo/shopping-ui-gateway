package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.NonNull;
import lombok.Value;

@Value
public class Category {
    @NonNull Long id;
    @NonNull Long restaurantId;
    @NonNull String name;
}
