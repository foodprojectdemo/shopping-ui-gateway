package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.NonNull;
import lombok.Value;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {
    Mono<String> createCart();

    Mono<Cart> getCart(String cartId);

    Mono<Void> addDishToCart(String cartId, Long dishId, int quantity);

    Mono<Void> changeCartItemQuantity(String cartId, int index, int quantity);

    Mono<Void> removeCartItem(String cartId, int index);

    Mono<Void> applyPromoCode(String cartId, String promoCodeId);

    Mono<Void> cancelPromoCode(String cartId);

    Mono<Void> placePreOrder(String cartId);

    Mono<Cart> getPreOrder(String cartId);

    Mono<String> signUpCustomer();

    Mono<Customer> getCustomer(String customerId);

    @Value
    class CheckoutRequest {
        @NonNull String street;
        @NonNull String house;
        @NonNull Integer flat;
        @NonNull String name;
        @NonNull String phone;
        @NonNull String email;
    }

    Mono<String> placeOrder(Mono<String> cartId, String customerId, CheckoutRequest request);

    Flux<Order> getOrders(String customerId);
}
