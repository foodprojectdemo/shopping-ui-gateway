package com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbsoluteDiscountPromoCode.class, name = "absolute_promo_code"),
        @JsonSubTypes.Type(value = PercentDiscountPromoCode.class, name = "percent_promo_code"),
})
public interface PromoCode {
}