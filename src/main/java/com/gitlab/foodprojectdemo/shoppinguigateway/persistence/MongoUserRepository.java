package com.gitlab.foodprojectdemo.shoppinguigateway.persistence;

import lombok.NonNull;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.User;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.UserRepository;
import reactor.core.publisher.Mono;

public interface MongoUserRepository extends ReactiveMongoRepository<User, User.CompositeUserId>, UserRepository {

    @SuppressWarnings("unchecked")
    Mono<User> save(@NonNull User user);

}
