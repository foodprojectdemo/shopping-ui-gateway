$(function () {
    $('.btn-remove-cart-item').click(function () {
        let row = $(this).closest(".row");
        $.ajax({
            type: "POST",
            url: "/cart/remove",
            data: {
                "index": row.data('index')
            },
            headers: {
                "X-CSRF-TOKEN": $('#checkout-form [name=_csrf]').val()
            },
            success: function (data) {
                location.reload();
            },
            error: errorHandler
        });
        return false;
    });
    $('[name=quantity]').change(function () {
        let row = $(this).closest(".row");
        $.ajax({
            type: "POST",
            url: "/cart/change",
            data: {
                "index": row.data('index'),
                "quantity": $(this).val()
            },
            headers: {
                "X-CSRF-TOKEN": $('#checkout-form [name=_csrf]').val()
            },
            success: function (data) {
                if (data.status === "SUCCESS") {
                    location.reload();
                } else if (data.status === "FAILURE") {
                    failureHandler(data.data)
                }
            },
            error: errorHandler
        });
        return false;
    });

    //TODO: check CSRF posting
    $('#promo-code-form').submit(function () {
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),
            headers: {
                "X-CSRF-TOKEN": $('#checkout-form [name=_csrf]').val()
            },
            success: function (data) {
                if (data.status === "SUCCESS") {
                    location.reload();
                } else if (data.status === "FAILURE") {
                    failureHandler(data.data)
                }
            },
            error: errorHandler
        });
        return false;
    });

    $('#checkout-form').submit(function () {
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),
            // headers: {
            //     "X-CSRF-TOKEN": $('#checkout-form [name=_csrf]').val()
            // },
            success: function (data) {
                if (data.status === "SUCCESS") {
                    location.href = "/checkout"
                } else if (data.status === "FAILURE") {
                    failureHandler(data.data)
                }
            },
            error: errorHandler
        });
        return false;
    });
});
