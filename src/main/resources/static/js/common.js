function errorHandler(jqXHR) {
    const err = JSON.parse(jqXHR.responseText);
    $('.toast .toast-body').text(err.data.message);
    $('.toast strong').text("Error");
    $('.toast .icon').attr("fill", "#ff0000");
    $('.toast').toast('show');
}

function failureHandler(data) {
    $('.toast .toast-body').text(data.message);
    $('.toast strong').text("Error");
    $('.toast .icon').attr("fill", "#ff0000");
    $('.toast').toast('show');
}

function notice(message) {
    $('.toast .toast-body').text(message);
    $('.toast strong').text("Notice");
    $('.toast .icon').attr("fill", "#007aff");
    $('.toast').toast('show');
}

$(function () {
    $(document).on('submit', '.add-to-cart-form', function () {
        const dish = $(this).closest(".dish");
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serialize(),
            success: function (data) {
                if (data.status === "FAILURE") {
                    failureHandler(data.data)
                } else {
                    notice(dish.find(".dish-name").text() + " added to your cart");
                }
            },
            error: errorHandler
        });
        return false;
    });
    $(".restaurant-navbar a").click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr("href")).offset().top - 80
        }, 1000);
    });
    // $('body').scrollspy({target: '.restaurant-navbar .navbar-brand.category', offset: -50})
});
