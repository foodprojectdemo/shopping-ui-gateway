package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;
import com.gitlab.foodprojectdemo.shoppinguigateway.config.WebClientConfig;
import com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order.OrderGateway.CheckoutParams;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;

@DirtiesContext
@SpringBootTest
@AutoConfigureJson
@ContextConfiguration(classes = {OrderGateway.class, WebClientConfig.class})
@TestPropertySource(properties = "order-service.url=http://localhost:${wiremock.server.port}/")
@AutoConfigureWireMock(stubs = "classpath:/META-INF/com.gitlab.foodprojectdemo/order-service/**/mappings/**/*.json", port = 0)
class OrderGatewayContractIT extends AbstractTest {

    @Autowired
    private OrderGateway orderGateway;

    @Test
    void createCart() {
        var cartId = orderGateway.createCart().block();
        assertThat(cartId).isNotNull();
    }

    @Test
    void getCart() {
        var cartDto = orderGateway.getCart(cartId()).block();
        assertThat(cartDto).isNotNull();
    }

    @Test
    void addDishToCart() {
        long dishId = faker().number().randomNumber();
        int quantity = faker().number().randomDigitNotZero();
        orderGateway.addDishToCart(cartId(), dishId, quantity).block();
    }

    @Test
    void changeCartItemQuantity() {
        int index = faker().number().randomDigitNotZero();
        int quantity = faker().number().randomDigitNotZero();
        orderGateway.changeCartItemQuantity(cartId(), index, quantity).block();
    }

    @Test
    void removeCartItem() {
        int index = faker().number().randomDigitNotZero();
        orderGateway.removeCartItem(cartId(), index).block();
    }

    @Test
    void applyPromoCode() {
        String promoCodeId = faker().lorem().word();
        orderGateway.applyPromoCode(cartId(), promoCodeId).block();
    }

    @Test
    void cancelPromoCode() {
        orderGateway.cancelPromoCode(cartId()).block();
    }

    @Test
    void placePreOrder() {
        orderGateway.placePreOrder(cartId()).block();
    }

    @Test
    void getPreOrder() {
        orderGateway.getPreOrder(cartId()).block();
    }

    @Test
    void signUpCustomer() {
        var customerId = orderGateway.signUpCustomer().block();
        assertThat(customerId).isNotNull();
    }

    @Test
    void getCustomer() {
        var customer = orderGateway.getCustomer(customerId()).block();
        assertThat(customer).isNotNull();
    }

    @Test
    void placeOrder() {
        var orderId = orderGateway.placeOrder(checkoutParams()).block();
        assertThat(orderId).isNotNull();
    }

    @Test
    void getOrders() {
        var orders = orderGateway.getOrders(customerId()).collectList().block();
        assertThat(orders).isNotEmpty();
    }

    private CheckoutParams checkoutParams() {
        return CheckoutParams.builder()
                .cartId(UUID.randomUUID().toString())
                .customerId(UUID.randomUUID().toString())
                .paymentMethodId(UUID.randomUUID().toString())
                .shippingMethodId(UUID.randomUUID().toString())
                .contactInformationName(faker().name().fullName())
                .contactInformationPhone(faker().phoneNumber().phoneNumber())
                .contactInformationEmail(faker().internet().emailAddress())
                .shippingAddressStreet(faker().address().streetName())
                .shippingAddressHouse(faker().number().digits(3))
                .shippingAddressFlat(faker().number().randomDigitNotZero())
                .build();

    }

    private String cartId() {
        return UUID.randomUUID().toString();
    }

    private String customerId() {
        return UUID.randomUUID().toString();
    }

}
