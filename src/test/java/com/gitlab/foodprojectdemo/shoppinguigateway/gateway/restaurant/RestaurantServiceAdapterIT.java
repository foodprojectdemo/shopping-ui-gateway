package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;
import com.gitlab.foodprojectdemo.shoppinguigateway.config.WebClientConfig;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.RestaurantService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PercentDiscountPromoAction;

import java.io.IOException;
import java.nio.file.Files;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;

@DirtiesContext
@SpringBootTest
@AutoConfigureJson
@ComponentScan
@ContextConfiguration(classes = {WebClientConfig.class})
@TestPropertySource(properties = "restaurant-service.url=http://localhost:${wiremock.server.port}/")
@AutoConfigureWireMock(port = 0)
class RestaurantServiceAdapterIT extends AbstractTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private WireMockServer wireMockServer;

    @Autowired
    private RestaurantService restaurantService;

    @Value("classpath:payload/restaurant-promo-actions-response.json")
    private Resource promoActionsResponse;

    @Value("classpath:payload/restaurant-dishes-response.json")
    private Resource dishesResponse;


    @Test
    void promoActions(Restaurant restaurant) throws IOException {
        wireMockServer.stubFor(
                get(urlEqualTo(String.format("/api/v1/restaurants/%s/promo-actions", restaurant.getId())))
                        .willReturn(
                                aResponse()
                                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                        .withBody(Files.readString(promoActionsResponse.getFile().toPath()))
                        )
        );
        wireMockServer.stubFor(
                get(urlEqualTo(String.format("/api/v1/restaurants/%s/dishes", restaurant.getId())))
                        .willReturn(
                                aResponse()
                                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                        .withBody(Files.readString(dishesResponse.getFile().toPath()))
                        )
        );

        var promoActions = restaurantService.promoActions(restaurant.getId()).collectList().block();

        assertThat(promoActions.size()).isEqualTo(3);
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(AbsoluteDiscountPromoAction.class));
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(PercentDiscountPromoAction.class));
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(GiftPromoAction.class));
    }
}