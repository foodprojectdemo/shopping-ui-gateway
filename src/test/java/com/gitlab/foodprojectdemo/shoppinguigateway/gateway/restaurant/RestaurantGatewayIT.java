package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;
import com.gitlab.foodprojectdemo.shoppinguigateway.config.WebClientConfig;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.AbsoluteDiscountPromoCode;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PercentDiscountPromoCode;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;

@DirtiesContext
@SpringBootTest
@AutoConfigureJson
@ComponentScan
@ContextConfiguration(classes = {WebClientConfig.class})
@TestPropertySource(properties = "restaurant-service.url=http://localhost:${wiremock.server.port}/")
@AutoConfigureWireMock(stubs = "classpath:/META-INF/com.gitlab.foodprojectdemo/restaurant-service/**/mappings/**/*.json", port = 0)
class RestaurantGatewayIT extends AbstractTest {

    @Autowired
    private RestaurantGateway restaurantGateway;

    @Test
    void restaurants() {
        var restaurants = restaurantGateway.restaurants().collectList().block();
        assertThat(restaurants).isNotEmpty();
    }

    @Test
    void categories() {
        var categories = restaurantGateway.categories(id()).collectList().block();
        assertThat(categories).isNotEmpty();
    }

    @Test
    void dishes() {
        var dishes = restaurantGateway.dishes(id()).collectList().block();
        assertThat(dishes).isNotEmpty();
    }

    @Test
    void dish() {
        restaurantGateway.dish(id()).block();
    }

    @Test
    void promoActions() {
        var promoActions = restaurantGateway.promoActions(id()).collectList().block();
        assertThat(promoActions.size()).isEqualTo(3);
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(AbsoluteDiscountPromoActionDto.class));
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(PercentDiscountPromoActionDto.class));
        assertThat(promoActions).anySatisfy(promoAction -> assertThat(promoAction).isInstanceOf(GiftPromoActionDto.class));
    }

    @Test
    void promoCodes() {
        var promoCodes = restaurantGateway.promoCodes(id()).collectList().block();
        assertThat(promoCodes.size()).isEqualTo(2);
        assertThat(promoCodes).anySatisfy(promoCode -> assertThat(promoCode).isInstanceOf(AbsoluteDiscountPromoCode.class));
        assertThat(promoCodes).anySatisfy(promoCode -> assertThat(promoCode).isInstanceOf(PercentDiscountPromoCode.class));
    }

    private Long id() {
        return faker().number().numberBetween(1L, 1000L);
    }
}
