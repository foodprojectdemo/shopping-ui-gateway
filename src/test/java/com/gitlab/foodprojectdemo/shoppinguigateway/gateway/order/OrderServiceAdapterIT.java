package com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;
import com.gitlab.foodprojectdemo.shoppinguigateway.config.WebClientConfig;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.OrderService;

import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DirtiesContext
@SpringBootTest
@AutoConfigureJson
@AutoConfigureWireMock(port = 0)
@ContextConfiguration(classes = WebClientConfig.class)
@TestPropertySource(properties = "order-service.url=http://localhost:${wiremock.server.port}/")
@ComponentScan({"com.gitlab.foodprojectdemo.shoppinguigateway.gateway.order", "com.gitlab.foodprojectdemo.shoppinguigateway.gateway.restaurant"})
class OrderServiceAdapterIT extends AbstractTest {

    @Autowired
    private OrderService orderService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private WireMockServer wireMockServer;

    @Value("classpath:payload/another-restaurant-dish-response.json")
    private Resource response;


    @Test
    void shouldThrowAnotherRestaurantDishException(Dish dish) throws IOException {
        var cartId = UUID.randomUUID().toString();
        wireMockServer.stubFor(
                post(urlPathMatching(String.format("/api/v1/carts/%s/items", cartId)))
                        .willReturn(
                                aResponse()
                                        .withStatus(403)
                                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                        .withBody(Files.readString(response.getFile().toPath()))
                        )
        );

        assertThatThrownBy(() -> orderService.addDishToCart(cartId, dish.getId(), 1).block())
                .isInstanceOf(AnotherRestaurantDishException.class)
                .hasMessage("You cannot add dishes from different restaurants into the cart. You can checkout order or clear your cart.");
    }
}
