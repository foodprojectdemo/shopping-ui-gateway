package com.gitlab.foodprojectdemo.shoppinguigateway;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;
import com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions.CartParameterResolver;
import com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions.CategoryParameterResolver;
import com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions.DishParameterResolver;
import com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions.PromoActionParameterResolver;
import com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions.RestaurantParameterResolver;

@ActiveProfiles("test")
@ExtendWith({
        MockitoExtension.class,
        CartParameterResolver.class,
        CategoryParameterResolver.class,
        DishParameterResolver.class,
        PromoActionParameterResolver.class,
        RestaurantParameterResolver.class
})
public class AbstractTest {
}
