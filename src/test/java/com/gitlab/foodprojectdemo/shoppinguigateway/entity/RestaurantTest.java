package com.gitlab.foodprojectdemo.shoppinguigateway.entity;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant.WeekDay;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class RestaurantTest extends AbstractTest {

    @Test
    void getOpenDays(Restaurant restaurant) {
        assertThat(restaurant.getOpenDays()).containsAll(Arrays.asList(WeekDay.values()));
    }
}