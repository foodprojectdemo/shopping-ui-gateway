package com.gitlab.foodprojectdemo.shoppinguigateway.fixture;

import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Category;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant.LegalData;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.AbsoluteDiscountPromoAction.Item;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;

import java.util.Collections;
import java.util.List;

import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;

public class RestaurantFixture {

    public static Restaurant restaurant() {
        return Restaurant.builder()
                .id(faker().number().randomNumber())
                .name(faker().name().title())
                .information(faker().lorem().paragraph())
                .logo(faker().internet().image())
                .openDays("1,2,3,4,5,6,7")
                .openFrom(String.format("%02d:00", faker().number().numberBetween(1, 12)))
                .openTo(String.format("%02d:00", faker().number().numberBetween(12, 24)))
                .legalData(legalData())
                .cuisines(Collections.singleton(faker().food().dish()))
                .build();
    }

    public static LegalData legalData() {
        return LegalData.builder()
                .name(faker().name().title())
                .address(faker().address().fullAddress())
                .inn(faker().number().digits(10))
                .ogrn(faker().number().digits(13))
                .build();
    }

    public static Category category() {
        return new Category(faker().number().randomNumber(), faker().number().randomNumber(), faker().food().dish());
    }

    public static PromoAction promoAction() {
        return new AbsoluteDiscountPromoAction(faker().number().randomNumber(), faker().number().randomNumber(),
                List.of(new Item(faker().number().randomDigit(), faker().number().randomDigit()))
        );
    }

    public static Dish dish() {
        return new Dish(
                faker().number().randomNumber(),
                faker().number().randomNumber(),
                faker().number().randomNumber(),
                faker().food().dish(),
                faker().lorem().paragraph(),
                faker().number().randomDigit(),
                faker().internet().image()
        );
    }
}
