package com.gitlab.foodprojectdemo.shoppinguigateway.fixture;

import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Cart;

import java.util.List;

import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;
import static com.gitlab.foodprojectdemo.shoppinguigateway.fixture.RestaurantFixture.dish;

public class OrderFixture {

    public static Cart cart() {
        return Cart
                .builder()
                .items(List.of(item()))
                .gifts(List.of(item()))
                .total(faker().number().digits(2))
                .subTotal(faker().number().digits(2))
                .discount(faker().number().digits(2))
                .promoCodeId(faker().number().digits(5))
                .build();
    }

    public static Cart.Item item() {
        return Cart.Item
                .builder()
                .dishId(faker().number().randomNumber())
                .quantity(faker().number().randomDigitNotZero())
                .price(faker().number().digits(2))
                .totalAmount(faker().number().digits(2))
                .dish(dish())
                .build();
    }

}
