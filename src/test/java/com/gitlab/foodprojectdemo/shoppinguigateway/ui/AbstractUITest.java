package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.reactive.server.WebTestClient.ResponseSpec;
import com.gitlab.foodprojectdemo.shoppinguigateway.AbstractTest;

abstract class AbstractUITest extends AbstractTest {
    @Autowired
    protected WebTestClient webClient;

    @MockBean
    private SignUpCustomerService signUpCustomerService;

    protected String getBody(ResponseSpec responseSpec) {
        return new String(responseSpec.expectBody().returnResult().getResponseBody());
    }
}
