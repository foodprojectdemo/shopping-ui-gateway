package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Category;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Restaurant;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.RestaurantService;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.promo.PromoAction;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.shoppinguigateway.Faker.faker;

@Slf4j
@WebFluxTest(RestaurantController.class)
class RestaurantControllerTest extends AbstractUITest {

    @MockBean
    private RestaurantService restaurantService;

    @Test
    void shouldShowRestaurantsPage(Restaurant restaurant) {
        when(restaurantService.restaurants(any())).thenReturn(Mono.just(new PageImpl<>(List.of(restaurant))));

        var body = getBody(webClient.get().uri("/")
                .exchange()
                .expectStatus().isOk());

        assertThat(body).contains(restaurant.getName());
    }

    @Test
    void shouldShowRestaurantPage(Restaurant restaurant, PromoAction promoAction) {
        var category = category(restaurant);
        var dish = dish(restaurant, category);
        when(restaurantService.restaurant(restaurant.getId())).thenReturn(Mono.just(restaurant));
        when(restaurantService.categories(restaurant.getId())).thenReturn(Flux.just(category));
        when(restaurantService.promoActions(restaurant.getId())).thenReturn(Flux.just(promoAction));
        when(restaurantService.dishes(restaurant.getId())).thenReturn(Flux.just(dish));


        var body = getBody(webClient.get().uri("/restaurant/{id}", restaurant.getId())
                .exchange()
                .expectStatus().isOk());
        log.info(body);
        assertThat(body).contains(restaurant.getName());
        assertThat(body).contains(dish.getName());
    }

    private Category category(Restaurant restaurant) {
        return new Category(faker().number().randomNumber(), restaurant.getId(), faker().food().dish());
    }

    private Dish dish(Restaurant restaurant, Category category) {
        return new Dish(
                faker().number().randomNumber(),
                restaurant.getId(),
                category.getId(),
                faker().food().dish(),
                faker().lorem().paragraph(),
                faker().number().randomDigit(),
                faker().internet().image());
    }
}
