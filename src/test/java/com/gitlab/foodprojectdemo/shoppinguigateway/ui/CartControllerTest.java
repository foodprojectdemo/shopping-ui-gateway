package com.gitlab.foodprojectdemo.shoppinguigateway.ui;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Cart;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Slf4j
@WebFluxTest(CartController.class)
class CartControllerTest extends AbstractUITest {

    @MockBean
    private CartService cartService;

    @Test
    void shouldShowCart(Cart cart) {
        when(cartService.getCart(any())).thenReturn(Mono.just(cart));

        var body = getBody(webClient.get().uri("/cart")
                .exchange()
                .expectStatus().isOk());

        assertThat(body).contains(cart.getItems().get(0).getDish().getName());
    }
}
