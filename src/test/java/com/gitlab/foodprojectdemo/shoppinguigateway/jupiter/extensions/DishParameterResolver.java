package com.gitlab.foodprojectdemo.shoppinguigateway.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.shoppinguigateway.entity.Dish;
import com.gitlab.foodprojectdemo.shoppinguigateway.fixture.RestaurantFixture;

public class DishParameterResolver extends TypeBasedParameterResolver<Dish> {
    @Override
    public Dish resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return RestaurantFixture.dish();
    }
}
