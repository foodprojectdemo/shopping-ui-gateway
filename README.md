# Shopping UI Gateway

Shopping UI Gateway - a UI Gateway for customers where they can order their favorite dishes. The gateway merges api
results of order and restaurant services.

### Development mode

1. Run infrastructure:

```shell
docker-compose up -d
```

2. Then run the gateway with the following command:

```shell
./mvnw spring-boot:run -DskipTests -Dspring-boot.run.profiles=dev 
```

The command runs the service with restaurant-stub.

### Authentication

The gateway provides authentication using OAuth through the providers: Google and GitHub. Also using in memory
UserProvider with credentials: ``user``/``password`` 
