SHELL=/bin/bash


update-contracts:
	./mvnw versions:use-latest-versions -Dincludes=com.gitlab.foodprojectdemo:restaurant-service-contracts \
		-DallowSnapshots=true
